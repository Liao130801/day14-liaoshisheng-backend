create table todo
(
    id   bigint auto_increment,
    name varchar(255) not null,
    done boolean      null,
    constraint todo_pk
        primary key (id)
);