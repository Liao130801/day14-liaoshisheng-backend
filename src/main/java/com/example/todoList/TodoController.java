package com.example.todoList;

import com.example.todoList.Entity.Todo;
import com.example.todoList.Service.TodoService;
import com.example.todoList.Service.dto.TodoRequest;
import com.example.todoList.Service.dto.TodoResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
@CrossOrigin
@RequestMapping("todos")
@RestController
public class TodoController {
    @Resource
    private TodoService todoService;
    @GetMapping
    public List<TodoResponse> getAllCompanies() {
        return todoService.findAll();
    }
    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable Long id) {
        return todoService.findById(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public TodoResponse updateTodo(@PathVariable Long id, @RequestBody TodoRequest todoRequest) {
        return todoService.update(id, todoRequest);
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        todoService.delete(id);
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse createTodo(@RequestBody TodoRequest TodoRequest) {
        return todoService.create(TodoRequest);
    }

}
