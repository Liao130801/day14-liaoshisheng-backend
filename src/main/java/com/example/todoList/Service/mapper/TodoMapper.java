package com.example.todoList.Service.mapper;

import com.example.todoList.Entity.Todo;
import com.example.todoList.Service.dto.TodoRequest;
import com.example.todoList.Service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class TodoMapper {
    public Todo toEntity(TodoRequest TodoRequest){
        Todo Todo = new Todo();
        BeanUtils.copyProperties(TodoRequest,Todo);
        Todo.setName(TodoRequest.getText());
        return Todo;
    }
    public TodoResponse toResponse(Todo Todo){
        TodoResponse response = new TodoResponse();
        BeanUtils.copyProperties(Todo,response);
        response.setText(Todo.getName());
        return response;
    }
}
