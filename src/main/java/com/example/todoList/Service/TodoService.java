package com.example.todoList.Service;

import com.example.todoList.Entity.Todo;
import com.example.todoList.Repository.TodoJPARepository;
import com.example.todoList.Service.dto.TodoRequest;
import com.example.todoList.Service.dto.TodoResponse;
import com.example.todoList.Service.mapper.TodoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TodoService {
    @Autowired
    private TodoJPARepository todoJPARepository;
    @Autowired
    private TodoMapper mapper;

    public TodoResponse create(TodoRequest TodoRequest) {
        TodoResponse TodoResponse = mapper.toResponse(todoJPARepository.save(mapper.toEntity(TodoRequest)));
        return  TodoResponse;
    }

    public List<TodoResponse> findAll() {
        return toResponseList(todoJPARepository.findAll());
    }

    public List<TodoResponse> toResponseList(List<Todo> todos) {
        List<TodoResponse> TodoResponses = new ArrayList<>();
        todos.stream().forEach(Todo -> {
            TodoResponse TodoResponse = mapper.toResponse(Todo);
            TodoResponses.add(TodoResponse);
        });
        return TodoResponses;
    }

    public TodoResponse findById(Long id) {
        Todo todo = todoJPARepository.findById(id).orElse(null);
        TodoResponse response = mapper.toResponse(todo);
        return response;
    }

    public TodoResponse update(Long id, TodoRequest todoRequest) {
        Optional<Todo> optionalTodo = todoJPARepository.findById(id);
        optionalTodo.ifPresent(previousTodo -> {
                        previousTodo.setName(todoRequest.getText());
                        previousTodo.setDone(todoRequest.getDone());

                }
        );
        todoJPARepository.save(optionalTodo.get());
        return findById(optionalTodo.get().getId());
    }

    public void delete(Long id) {
        todoJPARepository.deleteById(id);
    }
    

   
}
