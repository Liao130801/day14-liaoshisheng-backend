package com.example.todoList;

import com.example.todoList.Entity.Todo;
import com.example.todoList.Repository.TodoJPARepository;
import com.example.todoList.Service.dto.TodoRequest;
import com.example.todoList.Service.mapper.TodoMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.annotation.Resource;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class TodoListApplicationTests {
	@Autowired
	private MockMvc mockMvc;
	@Resource
	private TodoJPARepository todoJPARepository;
	@Resource
	private TodoMapper mapper;
	@BeforeEach
	void setUp() {
		todoJPARepository.deleteAll();
	}
	@Test
	void should_update_Todo_name_when_updateTodo_given_a_todo_request() throws Exception {
		TodoRequest todoRequest = new TodoRequest("abc",true);
		Long id = todoJPARepository.save(mapper.toEntity(todoRequest)).getId();
		TodoRequest todoUpdateRequest = new TodoRequest("xyz",true);
		ObjectMapper objectMapper = new ObjectMapper();
		String updatedTodoJson = objectMapper.writeValueAsString(todoUpdateRequest);
		mockMvc.perform(put("/todos/{id}", id)
						.contentType(MediaType.APPLICATION_JSON)
						.content(updatedTodoJson))
				.andExpect(MockMvcResultMatchers.status().is(204))
				.andExpect(MockMvcResultMatchers.jsonPath("$.text").value("xyz"));

		Optional<Todo> optionalTodo = todoJPARepository.findById(id);
		assertTrue(optionalTodo.isPresent());
		Todo updatedTodo = optionalTodo.get();
		Assertions.assertEquals(id, updatedTodo.getId());
		Assertions.assertEquals(todoUpdateRequest.getText(), updatedTodo.getName());
	}
	@Test
	void should_find_todos_when_findTodos_given_a_todo_request() throws Exception {
		Todo previousTodo = new Todo(1L, "abc", true);
		Todo savedTodo = todoJPARepository.save(previousTodo);
		mockMvc.perform(get("/todos"))
				.andExpect(MockMvcResultMatchers.status().is(200))
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedTodo .getId()))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(savedTodo.getName()))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(savedTodo.getDone()));
	}
	@Test
	void should_find_Todo_by_id_when_findById_given_a_todo_id() throws Exception {
		Todo previousTodo = new Todo(null, "abc", true);
		Todo saveTodo = todoJPARepository.save(previousTodo);

		mockMvc.perform(get("/todos/{id}", saveTodo.getId()))
				.andExpect(MockMvcResultMatchers.status().is(200))
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(saveTodo.getId()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.text").value(saveTodo.getName()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.done").value(saveTodo.getDone()));
	}
	@Test
	void should_delete_Todo_name_when_deleteTodo_given_a_todo_request() throws Exception {
		Todo Todo = new Todo(1L, "abc",true);
		Todo savedTodo = todoJPARepository.save(Todo);

		mockMvc.perform(delete("/todos/{id}", savedTodo.getId()))
				.andExpect(MockMvcResultMatchers.status().is(204));

		assertTrue(todoJPARepository.findById(savedTodo.getId()).isEmpty());
	}
	@Test
	void should_create_Todo_when_createTodo_given_a_todo_request() throws Exception {
		TodoRequest todoRequest = new TodoRequest("abc",true);

		ObjectMapper objectMapper = new ObjectMapper();
		String TodoRequest = objectMapper.writeValueAsString(todoRequest);
		mockMvc.perform(post("/todos")
						.contentType(MediaType.APPLICATION_JSON)
						.content(TodoRequest))
				.andExpect(MockMvcResultMatchers.status().is(201))
				.andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoRequest.getText()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoRequest.getDone()));
	}
	
}
